import React from 'react';
import './Navbar.css';
import { NavLink } from "react-router-dom";
import { BsCardList, BsWallet } from 'react-icons/bs';

const Navbar = () => {
    return (
        <footer>
        <nav>
            <NavLink className="nav-item" exact activeClassName="nav-active" to="/">
                 <BsCardList className="icon" /><p className="nav-text">Pokemon List</p>
            </NavLink>
            <NavLink className="nav-item" exact activeClassName="nav-active" to="/mypocket">
                 <BsWallet className="icon" />  <p className="nav-text">My Pokemon</p>
            </NavLink>
        </nav>
    </footer>
    )
}

export default Navbar;