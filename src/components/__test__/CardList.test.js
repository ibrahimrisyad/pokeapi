import CardList from '../CardList';
import { render, unmountComponentAtNode } from 'react-dom';
import { act } from "react-dom/test-utils";

let container = null;
beforeEach(() => {
    // setup a DOM element as a render target
    container = document.createElement("div");
    document.body.appendChild(container);
});

afterEach(() => {
    // cleanup on exiting
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});

it ('card list, with catched props', () => {
    act(() => {
        const pokemon = {
            name: 'Bulbasaur',
            catched: 3,
            image: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png'
        }
        render(<CardList name={pokemon.name} catched={pokemon.catched} image={pokemon.image} />, container)
    })
    expect(container).toMatchSnapshot()
})

it ('card list, with given_name props', () => {
    act(() => {
        const pokemon = {
            name: 'Bulbasaur',
            given_name: 'bulbaaaaa',
            image: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png'
        }
        render(<CardList name={pokemon.name} given_name={pokemon.given_name} image={pokemon.image} />, container)
    })
    expect(container).toMatchSnapshot()

    act(() => {
        const pokemon = {
            name: 'Bulbasaur',
            given_name: 'bulbaaaaa',
            image: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png'
        }
        render(<CardList name={pokemon.name} given_name={pokemon.given_name} image={pokemon.image} />, container)
    })
    expect(container.querySelector("button").textContent).toBe("Release")
})
