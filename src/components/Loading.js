import React from 'react';
import './Loading.css';

const Loading = (props) => {
    return (
        <div className="loading">
            <div>
                <img src="https://64.media.tumblr.com/da9c4314ebe0a33b852f6c3f38d3ab2d/tumblr_o4rgh6Jz5N1vpkaido1_400.gifv" alt="pidgey"></img>
                <h4>{props.msg}</h4>
            </div>
        </div>
    )
}

export default Loading