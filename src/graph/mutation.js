import { myPokemon } from '../cache';

const createPokemon = (pokemon) => {
    const currentPokemons = myPokemon();
    const newPokemon = pokemon
    localStorage.setItem('myPokemon', JSON.stringify([...currentPokemons, newPokemon]))
    myPokemon([...currentPokemons, newPokemon])
}

const releasePokemon = (given_name) => {
    const currentPokemons = myPokemon();
    const newPokemons = currentPokemons.filter((pokemon) => {
        return pokemon.given_name !== given_name
    })
    localStorage.setItem('myPokemon', JSON.stringify(newPokemons))
    myPokemon(newPokemons)
}

const editPokemon = (unique, given_name) => {
    const currentPokemons = myPokemon();
    const newPokemons = currentPokemons.filter((pokemon) => {
        if(pokemon.unique === unique) {
            pokemon.given_name=given_name
        }
        return pokemon
    })
    localStorage.setItem('myPokemon', JSON.stringify(newPokemons))
    myPokemon(newPokemons)
    // window.location.reload()
}
const Mutation = {releasePokemon, createPokemon, editPokemon}
export default Mutation