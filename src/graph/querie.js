import { myPokemon } from '../cache';
import { gql } from '@apollo/client';

const GET_POKEMON_DETAIL = gql`
    query pokemon($name: String!) {
        pokemon(name: $name) {
        id
        name
        abilities {
            ability {
                name
            }
        }
        moves {
            move {
                name
            }
        }
            types {
                type {
                    name
                }
            }
        }
    }
`
const GET_POKEMON_LIST = gql`
    query pokemons($limit: Int, $offset: Int) {
        pokemons(limit: $limit, offset: $offset) {
            count
    next
    previous
    status
    message
    results {
      url
      name
      image
      id
    }
        }
    }
`

const getMyPokemon = () => {
    return myPokemon()
}

const querie = {getMyPokemon, GET_POKEMON_DETAIL, GET_POKEMON_LIST}
export default querie