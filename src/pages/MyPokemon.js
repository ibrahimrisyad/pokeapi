import React, { useState } from 'react';
import querie from '../graph/querie';
import CardList from '../components/CardList';
import Mutation from '../graph/mutation';
import Empty from '../components/Empty';
import './MyPokemon.css';
import SuccessForm from '../components/SuccessForm';



const MyPokemon = () => {
    const [pokemonsState, setPokemonsState] = useState(querie.getMyPokemon())
    const [ editState, setEditState ] = useState([])
    const handleRelease = (e, given_name) => {
        e.stopPropagation()
       var answer = window.confirm("Are you sure want to release " + JSON.stringify(given_name) + " ?");
        if (answer) {
            Mutation.releasePokemon(given_name)
            setPokemonsState(querie.getMyPokemon())
        }
        
    }

    const handleRename = (e, pokemon) => {
        e.stopPropagation()
        setEditState(pokemon)
        // setPokemonsState(querie.getMyPokemon())
    }

    if(editState.name) {
        return (<SuccessForm pokemon={editState} />)
    }

    if (!pokemonsState.length) return (
        <>
            <h2 className="title">Catched Pokemon List</h2>
            <code>Total: {pokemonsState.length}</code>
            <Empty />
        </>
    )
    if (pokemonsState) {
        const pokemons = pokemonsState.map(({id, name, given_name, unique}) => {
            const imageUrl = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`
            return (
                <CardList
                    key={given_name}
                    name={name}
                    image={imageUrl}
                    given_name={given_name}
                    onRelease={handleRelease}
                    onRename={handleRename}
                    unique={unique}
                    id={id}
                />
            )
        })
        return (
            <>
                <h2 className="title">Catched Pokemon List</h2>
                <code>Total: {pokemonsState.length}</code>
                <div className="list">
                    {pokemons}
                </div>
            </>
        )
    }
}

export default MyPokemon;