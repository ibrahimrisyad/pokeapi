import React, { useState } from 'react';
import { useQuery } from '@apollo/client';
import querie  from '../graph/querie';
import { useParams } from 'react-router-dom';
import './PokemonDetail.css';
import Loading from '../components/Loading';
import Error from '../components/Error';
import SuccessForm from '../components/SuccessForm';

const PokemonDetail = () => {
    const { name } = useParams();
    const variables = { name }
    const { loading, error, data } = useQuery(querie.GET_POKEMON_DETAIL, { variables });
    const [catching, setCatching] = useState(false);
    const [success, setSuccess] = useState(false);

    const randomizer = () => Math.random() >= 0.5;

    const catchPokemon = () => {
        setCatching(true);

        setTimeout(() => {
        setCatching(false);
        setSuccess(randomizer());
        }, 3000);
    };

    if (loading) return <Loading msg='Please a moment...' />
    if (error) return <Error />
    if (success) {
        return (<SuccessForm pokemon={data.pokemon} />)
    }
    if (data) {
        const pokemon = data.pokemon;
        const imageUrl = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pokemon.id}.png`
        const listTypes = pokemon.types.map((pokemon, index) => {
            return (
                <span className={`type-${pokemon.type.name}`} key={index}>{pokemon.type.name}  </span>
            )
        })
        const listAbilities = pokemon.abilities.map((pokemon) => {
            return pokemon.ability.name
        })
        const listMoves = pokemon.moves.map((pokemon) => {
            return pokemon.move.name
        })
        var pokemonAbilities = listAbilities.map(function (i) {
            return (
            <span className='chip' key={i}>{i}</span>
            );
          });
        
        const pokemonMoves = listMoves.join(', ')
        return (
            <>
                <h2>Pokemon Detail</h2>
                <div className="detail-layout">
                    <div className="container">
                        <div className="img-wrapper">
                        <img className="pokemon-img" src={imageUrl} alt="pokemon"></img>
                        </div>

                        <button onClick={catchPokemon} className="Button button-detail">
                            {catching ? 'Catching...' : 'Catch'}
                        </button>
                    </div>
                    <div className="description-box">
                    <h3>{pokemon.name}</h3>
                        <div className="abilities-box">
                            <h4>Abilities:</h4>
                            <div className="p-wrapper">
                                <p>{pokemonAbilities}</p>
                            </div>
                        <div>
                        <h4 style={{marginBottom: '20px'}}>Type:</h4> 
                          
                        <h4 className="pokemon-text">{listTypes}</h4>
                        </div>
                        </div>
                        <div className="moves-box">
                            <h4>Available Moves:</h4>
                            <div className="p-wrapper">
                                <p>{pokemonMoves}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default PokemonDetail;