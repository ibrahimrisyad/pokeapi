import './App.css';
import React from 'react';
import PokemonList from './pages/PokemonList';
import MyPokemon from './pages/MyPokemon';
import PokemonDetail from './pages/PokemonDetail';
import Header from './components/Header';
import Navbar from './components/Navbar';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

function App() {
  localStorage.setItem('offset', 8)
  return (
    <Router>
      <div className="App">
        <Header />
        <hr  style={{
            color: 'rgb(220 220 220)',
            backgroundColor: 'rgb(220 220 220)',
            height: 1.5,
            borderColor : 'rgb(220 220 220)',
            marginTop: '-20px'
        }}/>
        <Switch>
          <Route exact path="/mypocket">
            <MyPokemon />
          </Route>
          <Route exact path="/pokemon/:name">
            <PokemonDetail />
          </Route>
          <Route exact path="/">
            <PokemonList />
          </Route>
        </Switch>
        <Navbar />
      </div>
    </Router>
  );
}

export default App;
